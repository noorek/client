import {FetchError} from "../types/Errors";
import axios from "../util/axios";
import {getError} from "../util/getError";

type fetch_object = "products" | "carts" | "cart_product";

export type State<T> = {
  data: T,
  loading: boolean,
  error: string,
}

export type FetchAction<T> = {
  type: "REQUEST"
} | {
  type: "SUCCESS",
  payload: T,
} | {
  type: "FAIL",
  payload: string,
}

export const reducer: <T>(state: State<T>, action: FetchAction<T>) => State<T> = (state, action) => {
  switch (action.type) {
    case "REQUEST":
      return {
        ...state, loading: true
      }
    case "SUCCESS":
      return {
        ...state, data: action.payload, loading: false
      }
    case "FAIL":
      return {
        ...state, loading: false, error: action.payload,
      }
    default:
      return state;
  }
}

export const fetchAll: <T>(object: fetch_object, dispatch: React.Dispatch<FetchAction<T>>) => Promise<void> = async (object, dispatch) => {
  dispatch({type: "REQUEST"});
  try {
    const result = await axios.get(`/${object}`);
    dispatch({type: "SUCCESS", payload: result.data})
  } catch (err) {
    dispatch({type: "FAIL", payload: getError(err as FetchError)})
  }
}