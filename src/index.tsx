import React from 'react'
import ReactDOM from 'react-dom/client'
import {createBrowserRouter, createRoutesFromElements, Route, RouterProvider} from "react-router-dom"
import App from './App.tsx'
import './index.css'
import HomePage from './pages/HomePage.tsx'
import Dashboard from './pages/Dashboard.tsx'

const router = createBrowserRouter(createRoutesFromElements(
  <Route path="/" Component={App}>
    <Route index={true} Component={HomePage} />
    <Route path="/dashboard" Component={Dashboard} />
    {/* <Route path="/login" Component={} /> */}
  </Route>
))

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
)
