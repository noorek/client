import Bar from '@material-ui/core/AppBar'
import {Container, Toolbar, Typography} from '@material-ui/core'
import {Link, Outlet} from 'react-router-dom'
import {HorizontalLayout} from './components/HorizontalLayout';



function App() {

  return (
    <div>
      <Bar style={{marginBottom: 20}} position='sticky'>
        <Toolbar>
          <HorizontalLayout align='center' gap={20}>
            <Link to={"/"} style={{textDecoration: "none", color: "inherit"}} >
              <Typography variant="h6">
                Store
              </Typography>
            </Link>
            <Link to={"/dashboard"} style={{textDecoration: "none", color: "inherit"}} >
              <Typography variant="h6">
                Dashboard
              </Typography>
            </Link>
          </HorizontalLayout>
        </Toolbar>
      </Bar>
      <Container >
        <Outlet />
      </Container>
    </div>
  )
}

export default App
