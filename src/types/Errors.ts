export declare type FetchError = {
  message: string,
  response?: {
    data?: {
      message: string,
    }
  }
}