import {Product} from "./Product";

type status = "pending" | "completed" | "rejected"

export type Cart = {
  id: string,
  cart_product_ids: string[],
  total_price: number,
  status: status
};

export type CartProduct = {
  cart_product_id: string,
  product_id: number,
  amount: number,
};

export type PendingCarts = {
  cart_id: string,
  products: Product[],
  total_price: number,
  status: "pending",
}