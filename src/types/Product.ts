export type Product = {
  id: number,
  name: string,
  image_url: string,
  price: number,
  amount: number,
}