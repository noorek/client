import * as React from "react";
import {Product} from "../types/Product";
import {IconButton, Divider} from "@material-ui/core";
import {HorizontalLayout} from "./HorizontalLayout";
import {Add} from "@material-ui/icons";

type ProductViewProps = {
  product: Product,
  handleAddToCart: (clickedProduct: Product) => void,
}

export const ProductView: React.FC<ProductViewProps> = ({product, handleAddToCart}) =>
  <div style={{
    backgroundColor: "white", display: "flex", flexDirection: "column",
    border: "3px solid lightblue", borderRadius: 20,
  }}>
    <img src={product.image_url} width={250} height={250} style={{objectFit: "cover", borderRadius: "20px 20px 0 0"}} />
    <Divider />
    <h3 style={{color: "black", margin: 10}}>{product.name}</h3>
    <Divider />
    <div style={{paddingLeft: 20, paddingRight: 20}}>
      <HorizontalLayout justify="space-between">
        <p style={{color: "black", fontWeight: "bold"}}>{product.price.toFixed(3)} KWD</p>
        <IconButton style={{outline: "none"}} onClick={() => handleAddToCart(product)}>
          <Add />
        </IconButton>
      </HorizontalLayout>
    </div>
  </div>