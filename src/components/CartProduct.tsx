import {Button} from "@material-ui/core";
import {VerticalLayout} from "./VerticalLayout";
import {HorizontalLayout} from "./HorizontalLayout";
import {Product} from "../types/Product";

type CartProductProps = {
  product: Product;
  addToCart: (clickedProduct: Product) => void;
  removeFromCart: (id: number) => void;
};

const CartProduct: React.FC<CartProductProps> = ({product, addToCart, removeFromCart}) => {
  return (
    <div style={{borderBottom: "1px solid lightblue", paddingBottom: 20, flex: 1}}>
      <HorizontalLayout align="center" gap={20}>
        <VerticalLayout fullWidth>
          <h3>{product.name}</h3>
          <HorizontalLayout justify="space-between">
            <p>Price: {product.price} KWD</p>
            <p>Total: {(product.amount * product.price).toFixed(2)} KWD</p>
          </HorizontalLayout>
          <HorizontalLayout justify="space-between">
            <Button
              size="small"
              disableElevation
              color="primary"
              variant="contained"
              onClick={() => removeFromCart(product.id)}
            >
              -
            </Button>
            <p>{product.amount}</p>
            <Button
              size="small"
              disableElevation
              color="primary"
              variant="contained"
              onClick={() => addToCart(product)}
            >
              +
            </Button>
          </HorizontalLayout>
        </VerticalLayout>
        <img src={product.image_url} width={100} />
      </HorizontalLayout>
    </div>
  );
};

export default CartProduct;