import * as React from "react";

type HorizontalLayout = {
  align?: "center",
  justify?: "center" | "space-between" | "flex-end",
  gap?: number,
  children?: React.ReactNode,
};

export const HorizontalLayout: React.FC<HorizontalLayout> = ({align, justify, gap, children}) =>
  <div style={{display: "flex", flexDirection: "row", justifyContent: justify, alignItems: align, gap, width: "100%"}}>
    {children}
  </div>