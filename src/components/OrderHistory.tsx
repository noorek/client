import * as React from "react";
import {PendingCarts} from "../types/Cart";
import {HorizontalLayout} from "./HorizontalLayout";
import {VerticalLayout} from "./VerticalLayout";
import {Accordion, AccordionSummary, Typography, AccordionDetails, ButtonGroup, Button} from "@material-ui/core";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import axios from "../util/axios";
import {FetchAction, fetchAll} from "../functions/fetch";

type OrderHistoryProps = {
  pendingCarts: PendingCarts[],
  dispatch: React.Dispatch<FetchAction<PendingCarts[]>>
}
export const OrderHistory: React.FC<OrderHistoryProps> = ({pendingCarts, dispatch}) => {
  if (pendingCarts.length === 0) {
    return <Typography variant="h3">No pending orders</Typography>
  }

  const onSubmit: (cart_id: string, status: "completed" | "rejected") => void = async (cart_id, status) =>
    axios.put(`/carts/update_status/${cart_id}`, {status})
      .then(() => fetchAll<PendingCarts[]>("carts", dispatch))
      .catch(err => console.log(err.response.data))

  return (pendingCarts.map(pendingCart =>
    <div key={pendingCart.cart_id} style={{borderBottom: "1px solid lightblue", paddingBottom: 20, flex: 1}}>
      <Accordion>
        <AccordionSummary style={{backgroundColor: "lightBlue"}} expandIcon={<ExpandMoreIcon />}>
          <HorizontalLayout justify="space-between">
            <Typography variant="h5">Cart: {pendingCart.cart_id.substring(0, 6).toUpperCase()}</Typography>
            <Typography variant="h6">Total Price: {pendingCart.total_price} KWD</Typography>
          </HorizontalLayout>
        </AccordionSummary>
        <AccordionDetails>
          <VerticalLayout fullWidth align="center">{
            pendingCart.products.map(product =>
              <HorizontalLayout key={product.id} align="center" gap={20}>
                <img src={product.image_url} width={100} />
                <h3>{product.name}</h3>
                <p>Price: {product.price} KWD</p>
                <p>Quantity: {product.amount}</p>
              </HorizontalLayout>
            )}
            <HorizontalLayout justify="flex-end">
              <ButtonGroup>
                <Button style={{outline: "none"}} size="large" variant="contained" color="primary" onClick={() => onSubmit(pendingCart.cart_id, "completed")}>
                  Complete
                </Button>
                <Button style={{outline: "none"}} size="large" variant="contained" color="secondary" onClick={() => onSubmit(pendingCart.cart_id, "rejected")}>
                  Reject
                </Button>
              </ButtonGroup>
            </HorizontalLayout>
          </VerticalLayout>
        </AccordionDetails>
      </Accordion>
    </div>
  ))
}