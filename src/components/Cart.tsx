import {Button} from "@material-ui/core";
import {Product} from "../types/Product";
import CartProduct from "./CartProduct";

type CartProps = {
  cartProducts: Product[];
  addToCart: (clickedProduct: Product) => void;
  removeFromCart: (id: number, removeAll?: boolean) => void;
  onCheckout: () => void;
};


const Cart: React.FC<CartProps> = ({cartProducts, addToCart, removeFromCart, onCheckout}) => {
  const calculateTotal = (products: Product[]) =>
    products.reduce((acc, product) => acc + product.amount * product.price, 0);

  return (
    <div style={{width: 500, padding: 20}}>
      <h2>Your Cart</h2>
      {cartProducts.length === 0 ? <p>No products in cart.</p> : null}
      {cartProducts.map((product) => (
        <CartProduct
          key={product.id}
          product={product}
          addToCart={addToCart}
          removeFromCart={removeFromCart}
        />
      ))}
      <h2>Total: {calculateTotal(cartProducts).toFixed(3)} KWD</h2>
      <Button
        color="primary"
        variant="contained"
        fullWidth
        disabled={cartProducts.length === 0}
        onClick={onCheckout}>
        Checkout
      </Button>
    </div>
  );
};

export default Cart;