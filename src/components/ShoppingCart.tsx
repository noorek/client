import * as React from "react";
import {Badge} from "@material-ui/core";
import {AddShoppingCart} from "@material-ui/icons";

type ShoppingCartProps = {
  setCartOpen: (open: boolean) => void,
  totalProducts: number,
}

export const ShoppingCart: React.FC<ShoppingCartProps> = ({setCartOpen, totalProducts}) =>
  <div style={{position: "fixed", zIndex: 10000, right: 20, top: 20}} onClick={() => setCartOpen(true)}>
    <Badge badgeContent={totalProducts} color="error">
      <AddShoppingCart />
    </Badge>
  </div>