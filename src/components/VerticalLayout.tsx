import * as React from "react";

type VerticalLayoutProps = {
  align?: "center",
  justify?: "center",
  fullWidth?: boolean,
  gap?: number,
  children?: React.ReactNode,
};

export const VerticalLayout: React.FC<VerticalLayoutProps> = ({align, justify, gap, fullWidth, children}) =>
  <div style={{display: "flex", flexDirection: "column", justifyContent: justify, alignItems: align, gap, width: fullWidth ? "100%" : undefined}}>
    {children}
  </div>