import React from 'react';
import {ProductView} from './ProductView';
import {Product} from '../types/Product';
import {Grid} from '@material-ui/core';

type ProductListProps = {
  products: Product[],
  handleAddToCart: (clickedProduct: Product) => void,
}

export const ProductList: React.FC<ProductListProps> = ({products, handleAddToCart}) => {
  return (
    <Grid container spacing={5}>
      {products.map(product => (
        <Grid item key={product.id}>
          <ProductView handleAddToCart={handleAddToCart} product={product} />
        </Grid>
      ))}
    </Grid>
  );
};