import * as React from "react";
import {useReducer} from "react";
import {PendingCarts} from "../types/Cart";
import {FetchAction, State, reducer, fetchAll} from "../functions/fetch";
import {CircularProgress} from "@material-ui/core";
import {HorizontalLayout} from "../components/HorizontalLayout";
import {OrderHistory} from "../components/OrderHistory";



type CartState = State<PendingCarts[]>;

const initialState: CartState = {
  data: [],
  loading: true,
  error: ""
}

const Dashboard: React.FC = () => {
  const [state, dispatch] = useReducer<React.Reducer<State<PendingCarts[]>, FetchAction<PendingCarts[]>>>(reducer, initialState);
  React.useEffect(() => {fetchAll<PendingCarts[]>("carts", dispatch)}, []);
  if (state.loading) return <HorizontalLayout justify="center"><CircularProgress /></HorizontalLayout>
  return <OrderHistory pendingCarts={state.data} dispatch={dispatch} />
}

export default Dashboard