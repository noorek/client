import * as React from "react";
import {ProductList} from "../components/ProductList";
import {Product} from "../types/Product";
import {useReducer} from "react";
import axios from "../util/axios";
import {CircularProgress, Drawer} from "@material-ui/core";
import {Alert} from '@material-ui/lab';
import {useState} from 'react';
import Cart from '../components/Cart';
import {ShoppingCart} from '../components/ShoppingCart';
import {FetchAction, State, reducer, fetchAll} from "../functions/fetch";

type ProductState = State<Product[]>;

const initialState: ProductState = {
  data: [],
  loading: true,
  error: ""
}

const HomePage: React.FC = () => {
  const [state, dispatch] = useReducer<React.Reducer<State<Product[]>, FetchAction<Product[]>>>(reducer, initialState);


  const [cartOpen, setCartOpen] = useState(false);
  const [cartProducts, setCartProducts] = useState<Product[]>([]);


  const getTotalProducts = (products: Product[]) =>
    products.reduce((acc, product) => acc + product.amount, 0);

  const handleAddToCart = (clickedProduct: Product) => {
    setCartProducts((prev) => {
      const isProductInCart = prev.find((item) => item.id === clickedProduct.id);
      if (isProductInCart) {
        return prev.map((item) =>
          item.id === clickedProduct.id
            ? {...item, amount: item.amount + 1}
            : item
        );
      }

      return [...prev, {...clickedProduct, amount: 1}];
    });
  };

  const handleRemoveFromCart = (id: number) => {
    setCartProducts((prev) =>
      prev.reduce((acc, product) => {
        if (product.id === id) {
          if (product.amount === 1) return acc;
          return [...acc, {...product, amount: product.amount - 1}];
        } else {
          return [...acc, product];
        }
      }, [] as Product[])
    );
  };

  const handleCheckout = () => {
    axios.post("/checkout/", cartProducts);
    setCartProducts([]);
  }

  React.useEffect(() => {fetchAll<Product[]>("products", dispatch)}, []);
  if (state.loading) return <CircularProgress />;
  if (state.error) return <Alert severity="error">{state.error}</Alert>
  return <div>
    <Drawer style={{zIndex: 10001}} anchor="right" open={cartOpen} onClose={() => setCartOpen(false)}>
      <Cart
        cartProducts={cartProducts}
        addToCart={handleAddToCart}
        removeFromCart={handleRemoveFromCart}
        onCheckout={handleCheckout}
      />
    </Drawer>
    <ShoppingCart setCartOpen={setCartOpen} totalProducts={getTotalProducts(cartProducts)} />
    <ProductList handleAddToCart={handleAddToCart} products={state.data} />
  </div>
}

export default HomePage;