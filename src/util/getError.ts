import {FetchError} from "../types/Errors";

export const getError = (error: FetchError) => {
  const message = error.response?.data?.message || error.message;
  return message;
}