import axios from "axios";

const baseURL = process.env.NODE_ENV === "development"
  ? "http://localhost:4001/"
  : "/"

export default axios.create({
  baseURL: baseURL + 'api'
});